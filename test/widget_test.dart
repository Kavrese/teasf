// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:new_sessions/data/storage/get_onboarding_items.dart';
import 'package:new_sessions/domain/queue_use_case.dart';

import 'package:new_sessions/main.dart';
import 'package:new_sessions/presentation/pages/onbording.dart';

void main() {
  group('Testing Onboarding', () {
    test('Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь)', () {
      initQueue();
      for (var val in getOnboardingItems()){
        expect(next(), jsonDecode(val));
      }
    });
    test('Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).', () {
      initQueue();
      int lenght_queue = len();
      for (var val in getOnboardingItems()){
        next();
        expect(len(), lenght_queue - 1);
        lenght_queue = len();
      }
    });
    testWidgets('В случае, когда в очереди меньше 2 картинок, устанавливается правильная надпись на кнопке.(Вместо Начать - Далее)', (widgetTester) async{
      initQueue();
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MaterialApp(
        home: OnBoarding(),
      )));

      //var useCase = widgetTester.widget<OnBoarding>(find.byType(OnBoarding)).useCase;

      while (len() >= 2){
        await widgetTester.tap(find.widgetWithText(FilledButton, 'Начать'));

        await widgetTester.pumpAndSettle();
      }
      expect(find.widgetWithText(FilledButton, 'Далее'), findsOneWidget);

    });
    testWidgets('Случай, когда в очереди два изображения, надпись на кнопке должна быть Начать', (widgetTester) async{
      initQueue();
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MaterialApp(
        home: OnBoarding(),
      )));
      initQueue();
      while (len() > 2){
        await widgetTester.tap(find.byType(FilledButton));
        await widgetTester.pumpAndSettle();
      }
      expect(find.widgetWithText(FilledButton, 'Начать'), findsOneWidget);

    });
    testWidgets('Если очередь пустая и пользователь нажал на кнопку “Далее”, происходит открытие экрана «Sign Up» приложения. Если очередь не пустая – переход отсутствует.', (widgetTester) async{
      initQueue();
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MaterialApp(
        home: OnBoarding(),
      )));
      initQueue();
      while (len() != 0){
        await widgetTester.tap(find.byType(FilledButton));
        await widgetTester.pumpAndSettle();
        expect(find.byKey(Key('Sign Up')), findsNothing);
      }
      await widgetTester.tap(find.widgetWithText(FilledButton, 'Далее'));
      await widgetTester.pumpAndSettle();

      expect(find.byKey(Key('SignUp'),), findsOneWidget);

    });

  });
}

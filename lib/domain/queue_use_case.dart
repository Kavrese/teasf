
import 'dart:async';

import 'package:new_sessions/data/storage/queue.dart';


  int len(){
    return queue.len();
  }

  bool isEmpty(){
    return queue.isEmpty();
  }

  Future<void > initQueue()async{
    await queue.loadQueue();
  }

Map<String, dynamic> next() {
    return queue.next();
  }

Future<void> makeEndQueue()async{
    await queue.makeEndQueue();
}

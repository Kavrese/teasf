import 'package:flutter/material.dart';
import 'package:new_sessions/data/repository/shared_preferences.dart';
import 'package:new_sessions/data/storage/queue.dart';
import 'package:new_sessions/presentation/pages/onbording.dart';
import 'package:new_sessions/presentation/pages/sign_up.dart';

bool success = false;
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await initSharedPreferences();
  if (queue.getEndQueue()){
    success = true;
  }else{
    await queue.loadQueue();
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          title: 'Flutter Demo',
          home: (success)?SignUp():OnBoarding()
        );
  }
}


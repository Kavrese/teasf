import 'dart:convert';

import 'package:new_sessions/data/storage/get_onboarding_items.dart';
import 'package:new_sessions/data/storage/queue.dart';
import 'package:new_sessions/data/storage/shared_preference.dart';

class Queue {
  List<String> sp = [];

  Future<void> loadQueue() async{
    sp.clear();
    sp = getOnboardingItems();
    int actualLen = sharedPreferences?.getInt('actualLen') ?? -1;
    if (actualLen == -1){
      await sharedPreferences?.setInt('actualLen', sp.length);
    }
    else{
      for (int i = 0; i < (sp.length - actualLen); i++){
        queue.next();
      }
    }
    print(sharedPreferences?.getInt('actualLen'));
  }


  Map<String, dynamic> next(){
    final element = sp[0];
    sp.remove(element);
    sharedPreferences?.setInt('actualLen', sp.length);
    return jsonDecode(element);
  }

  Future<void> makeEndQueue()async{
    await sharedPreferences?.setBool('isEndQueue', true);
  }

  bool getEndQueue(){
    return sharedPreferences?.getBool('isEndQueue') ?? false;
  }


  bool isEmpty() {
    return sp.isEmpty;
  }

  int len() {
    return sp.length;
  }
}







